# Genie_poc



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Commands run
```
julia

julia> ]
pkg> add Genie

julia>Genie.Generator.newapp_webservice("webservice")

julia>Genie.Generator.newapp_mvc("MyMVC")
julia>include(joinpath("config", "initializers", "searchlight.jl"))
julia> Genie.Generator.newresource("anne")
julia> SearchLight.Migration.new_table("add_anne_table", "anne")
julia> SearchLight.Migration.create_migrations_table()
julia> SearchLight.Migration.status()
julia> a =  Anne(year=2023, cost=23000.0)
julia> save(a)
```
